module.exports = {
    isUserNameValid: function(username){
        if(username.length<3 || username.length>15){
            return false;
        }
        if(username.toLowerCase() !== username){
            return false;
        }
        return true;
    },
    isAgeValid: function(age){
        
        if(age.match(/^([a-z])+$/i) ){
            return false;
        }
        if(age <18 || age > 100){
            return false;
        }
        return true;
    },
    isPasswordValid: function(password){
        if(password.length < 8){
            return false;
        }
        if(!/[A-Z]/.test(password) ){
            return false;
        }
        countInt = 0;
        for(let i = 0; i < password.length;i++){
            if(password.charAt(i).match(/[0-9]/) ){
                countInt++;
            }
            
        } if(countInt < 3){
            return false;
        }
        var cahSpecial = /[!@#\$%\^\&*\)\(+=._-]/g
            if(!cahSpecial.test(password)) {
                return false;
            }
        return true;
    },
    isDateValid: function(day, month, year){
        if(day < 1 || day > 31) return false;
        if(month < 1 || month > 12) return false;
        if(year < 1970 || year > 2020) return false;

        switch (month) {
            case 1, 3, 5, 7, 8, 10, 12:
                if (day < 1 || day >31) {
                    return false;
                }
                break;
            case 4, 6, 9, 11:
                if (day < 1|| day > 30) {
                    return false; 
                }
                break;
            case 2:
                if (year % 4 == 0) {
                    if (year % 100 == 0 ) {
                        if (year % 400 == 0) {
                            if (day > 29) { 
                                return false;
                            }
                        } else {
                            if(day > 28) {
                                return false;
                            }
                        }
                    } else{
                        if(day > 29) {
                            return false;
                        }
                    }
                } else {
                    if(day > 28) {
                        return false;
                    }
                }

               break;
            default:
                break;
        }
        return true;
    }
}